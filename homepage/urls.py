from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.Index, name='Index'),
    path('MyProfile', views.MyProfile, name='MyProfile'),
    path('Hobbies', views.Hobbies, name='Hobbies'),
    path('Education', views.Education, name='Education'),
    path('ExperienceAndSkills', views.ExperienceAndSkills, name='ExperienceAndSkills'),
    path('Favourites', views.Favourites, name='Favourites'),
    path('Story1', views.Story1, name='Story1'),
]
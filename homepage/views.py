from django.shortcuts import render

# Create your views here.
def Index(request):
    return render(request, 'MyProfile.html')

def MyProfile(request):
    return render(request, 'MyProfile.html')

def Hobbies(request):
    return render(request, 'Hobbies.html')

def Education(request):
    return render(request, 'Education.html')

def ExperienceAndSkills(request):
    return render(request, 'ExperienceAndSkills.html')

def Favourites(request):
    return render(request, 'Favourites.html')

def Story1(request):
    return render(request, 'Story1.html')